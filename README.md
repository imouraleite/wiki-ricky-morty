This is a project created with [Next.js](https://nextjs.org/)

Execute below command to install the dependencies:

```bash
npm install
# or
yarn install
```

Run the development server:

```bash
npm run dev
# or
yarn dev
```
