import Head from "next/head";
import Image from "next/image";
import styles from "@/styles/Home.module.css";
import { CharacterCard } from "@/components/card";

const MAX_CHARACTERS = 826;
const MIN_CHARACTERS = 1;

const CHARACTER = process.env.NEXT_PUBLIC_CHARACTER || "";

function getCharacterIdRandom(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getSixCharacterIdRandoms() {
  let arrayDeSeisNumeros = [];
  for (let i = 0; i < 6; i++) {
    arrayDeSeisNumeros.push(getCharacterIdRandom(MIN_CHARACTERS, MAX_CHARACTERS));
  }
  return arrayDeSeisNumeros;
}

export async function getServerSideProps() {
  const cards_home = `${CHARACTER}[${getSixCharacterIdRandoms()}]`;
  const f = await fetch(cards_home);
  const data = await f.json();
  return {
    props: {
      characters: data,
    },
  };
}

export default function Home({ characters }) {
  return (
    <>
      <Head>
        <title>Ricky and Morty Wiki</title>
        <meta
          name="description"
          content="Wiki for Ricky and Morty created with Next.js"
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className={styles.main_title}>
        <h1>Ricky and Morty Wiki</h1>
      </div>
      <div className={styles.main_card}>
        <div className={styles.main_result_card}>
            {characters.map((character) => (
                <CharacterCard key={character.id} pageProps={character} />
            ))}
        </div>
      </div>
    </>
  );
}
