import { useState } from "react";
import FilterResults from "../../components/filterResults";
import { filter } from "../../util/util";

import styles_filter from "../../styles/Character.module.css";
import styles_card_container from "../../styles/Home.module.css";
import { LocationCard } from "@/components/card";

const LOCATION = process.env.NEXT_PUBLIC_LOCATION || "";

export default function Location(){
    const [results, setResults] = useState({});

    const fetchResults = async (url) => {
        const f = await fetch(url);
        const data = await f.json();
    
        if(!data.results){
            data.noresults = "No results found";
        }
        setResults(data);
    }

    const nextPrevPagination = async (url) => await fetchResults(url);

    // Handles the submit event on form submit.
    const handleSubmit = async (event) => {
        // Stop the form from submitting and refreshing the page.
        event.preventDefault();

        // Get data from the form.
        const filters = {
            name: event.target.name.value,
            type: event.target.type.value,
            dimension: event.target.dimension.value,
        };

        const cards_home = `${LOCATION}${ filter(filters) }`;
        fetchResults(cards_home);

    };
    return(

        <>
            <form onSubmit={handleSubmit} >
                <div className={ styles_filter.filter }>
                    <h1>Location's Filter</h1>
                    <div className={ styles_filter.filter_form}>
                        <div>
                            <div className={ styles_filter.filter_div_input}><p>Name:</p><input type="text" id="name" name="name" /></div>
                            <div className={ styles_filter.filter_div_input}><p>Type:</p><input type="text" id="type" name="type" /></div>
                            <div className={ styles_filter.filter_div_input}><p>Dimension:</p><input type="text" id="dimension" name="dimension" /></div>
                        </div>
                    </div>
                </div>
                <div className={styles_filter.search}>
                    <button type="submit">Search</button>
                </div>
            </form>
            
            <div className={results.noresults ? styles_card_container.main_no_results : styles_card_container.main_card}>
                <FilterResults pageProps={results} pagination={nextPrevPagination}>
                    <LocationCard />
                </FilterResults>
            </div>
        </>
    )

}