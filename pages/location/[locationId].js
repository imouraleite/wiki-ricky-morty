import { CharacterCard } from "@/components/card";

import styles from "../../styles/Location.module.css";

const LOCATION = process.env.NEXT_PUBLIC_LOCATION || "";

export async function getServerSideProps(context) {
    const id = context.params.locationId;
    const res = await fetch(`${LOCATION}${id}`);
    const data = await res.json();

    //return a object of resident
    const residents = data.residents.map( resident => getResident(resident));
    data.residents = await Promise.all(residents);

    return {
      props: { location: data },
    };
}

async function getResident(url){
    const resident = await fetch(url).then(residentData => residentData.json());
    return resident;
}

export default function Location({ location }) {
    return (
        <>
            <div>
                <h1 className={styles.title}>{location.name}</h1>
            </div>
            <div className={styles.location}>
                <p><span>Type: </span> {location.type}</p>
                <p><span>Dimension: </span> {location.dimension}</p>
            </div>
            <div>
                <div className={styles.residents_title}>
                    <h2>Residents</h2>
                </div>
                <div className={styles.resident_cards}>
                    {
                        location.residents.map(
                            resident => 
                                <CharacterCard pageProps={resident} />
                        )
                    }
                </div> 
            </div>
        </>
    );
}