import { useState } from "react";
import Dropdown from "../../components/dropdown";
import FilterResults from "../../components/filterResults";
import { CharacterCard } from "@/components/card";
import { filter } from "@/util/util"

import styles_filter from "../../styles/Character.module.css";
import styles_card_container from "../../styles/Home.module.css";

const CHARACTER = process.env.NEXT_PUBLIC_CHARACTER;

const STATUS = ["alive", "dead", "unknown"];
const GENDER = ["female", "male", "genderless", "unknown"];

export default function Character() {

    const [results, setResults] = useState({});

    const fetchResults = async (url) => {
        const f = await fetch(url);
        const data = await f.json();
    
        if(!data.results){
            data.noresults = "No results found";
        }
        setResults(data);
    }

    const nextPrevPagination = async (url) => await fetchResults(url);

    // Handles the submit event on form submit.
    const handleSubmit = async (event) => {
        // Stop the form from submitting and refreshing the page.
        event.preventDefault();

        // Get data from the form.
        const filters = {
            name: event.target.name.value,
            status: event.target.Status.value,
            gender: event.target.Gender.value,
        };

        const cards_home = `${CHARACTER}${ filter(filters) }`;
        fetchResults(cards_home);

    };
    return (
        <>
                <form onSubmit={handleSubmit} >
                    <div className={ styles_filter.filter }>
                        <h1>Character's Filter</h1>
                        <div className={ styles_filter.filter_form}>
                            <div>
                                <div className={ styles_filter.filter_div_input}><p>Name:</p><input type="text" id="name" name="name" /></div>
                            </div>
                            <div>
                                <Dropdown options={ STATUS } name={ "Status" }/>                   
                                <Dropdown options={ GENDER } name={ "Gender" }/>
                            </div>
                        </div>
                    </div>
                    <div className={styles_filter.search}>
                        <button type="submit">Search</button>
                    </div>
                </form>
            
            <div className={results && results.results ? styles_card_container.main_card : styles_card_container.main_no_results}>
                <FilterResults pageProps={results} pagination={nextPrevPagination}>
                    <CharacterCard />
                </FilterResults>
            </div>
        </>
    );
}

