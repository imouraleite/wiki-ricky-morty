import Image from "next/image";
import Link from "next/link"

import styles from "../../styles/Character.module.css";

const LOCATION = process.env.NEXT_PUBLIC_LOCATION || "";
const CHARACTER = process.env.NEXT_PUBLIC_CHARACTER || "";

function getIdLocation ( episodeURL ) {
    const PATTERN = LOCATION;
    return episodeURL.replace(PATTERN, "");
}

export async function getServerSideProps(context) {
  const id = context.params.characterId;
  const res = await fetch(`${CHARACTER}${id}`);
  const data = await res.json();
  return {
    props: { character: data },
  };
}

export default function Character({ character }) {
  return (
    <>
        <div className={styles.character_container}>
            <h1 className={styles.title}>{character.name}</h1>
            <Image
                src={`${CHARACTER}avatar/${character.id}.jpeg`}
                width="200"
                height="200"
                alt={character.name}
            />
            <div>
                <span className={`${styles.status} ${styles["status_" + character.status.toLowerCase()]}`}>
                    {character.status}
                </span>
            </div>
        </div>
        <div className={styles.data_container}>
            <div>
                <h3>ID:</h3>
                <p>#{character.id}</p>
            </div>
            <div>
                <h3>Species:</h3>
                <p>{character.species}</p>
            </div>
            <div>
                <h3>Gender:</h3>
                <p>{character.gender}</p>
            </div>
            <div>
                <h3>Type:</h3>
                <p>{character.type == "" ? "-" : character.type}</p>
            </div>
        </div>
        <div className={styles.data_container}>
            {
                character.origin.url === "" ?
                <div>
                    <h3>Origin Location:</h3>
                    <p>{character.origin.name}</p>
                </div>
                :
                <div>
                    <h3>Origin Location:</h3>
                    <p>
                        <Link href={`/location/${ getIdLocation( character.origin.url ) }`}>
                            {character.origin.name}
                        </Link>
                    </p>
                </div>
            }
           {
                character.location.url === "" ?
                <div>
                    <h3>Current Location:</h3>
                    <p>{character.location.name}</p>
                </div>
                :
                <div>
                    <h3>Current Location:</h3>
                    <p>
                        <Link href={`/location/${ getIdLocation( character.location.url ) }`}>
                            {character.location.name}
                        </Link>
                    </p>
                </div>
            }
        </div>
    </>
  );
}
