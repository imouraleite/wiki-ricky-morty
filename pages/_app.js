import MainContainer from '@/components/mainContainer';
import '@/styles/globals.css'

export default function App({ Component, pageProps }) {
  return (
      <MainContainer data={ ...Component.name}>
        <Component {...pageProps} />
      </MainContainer>
    );
}
