import { CharacterCard } from "@/components/card";

import styles from "../../styles/Location.module.css";

const EPISODE = process.env.NEXT_PUBLIC_EPISODE || "";

export async function getServerSideProps(context) {
    const id = context.params.episodeId;
    const res = await fetch(`${EPISODE}${id}`);
    const data = await res.json();

    //return a object of resident
    const characters = data.characters.map( character => getEpisode(character));
    data.characters = await Promise.all(characters);

    return {
      props: { episode: data },
    };
}

async function getEpisode(url){
    const episode = await fetch(url).then(episodeData => episodeData.json());
    return episode;
}

export default function Episode({ episode }) {
    return (
        <>
            <div>
                <h1 className={styles.title}>{episode.name}</h1>
            </div>
            <div className={styles.location}>
                <p><span>Air Date: </span> {episode.air_date}</p>
                <p><span>Episode: </span> {episode.episode}</p>
            </div>
            <div>
                <div className={styles.residents_title}>
                    <h2>Characters</h2>
                </div>
                <div className={styles.resident_cards}>
                    {
                        episode.characters.map(
                            character => 
                                <CharacterCard pageProps={character} />
                        )
                    }
                </div> 
            </div>
        </>
    );
}