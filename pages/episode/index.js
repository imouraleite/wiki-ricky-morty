import { useState } from "react";
import FilterResults from "../../components/filterResults";
import { filter } from "../../util/util";

import styles_filter from "../../styles/Character.module.css";
import styles_card_container from "../../styles/Home.module.css";
import { EpisodeCard } from "@/components/card";

const EPISODE = process.env.NEXT_PUBLIC_EPISODE || "";

export default function Episode(){
    const [results, setResults] = useState({});

    const fetchResults = async (url) => {
        const f = await fetch(url);
        const data = await f.json();
    
        if(!data.results){
            data.noresults = "No results found";
        }
        setResults(data);
    }

    const nextPrevPagination = async (url) => await fetchResults(url);

    // Handles the submit event on form submit.
    const handleSubmit = async (event) => {
        // Stop the form from submitting and refreshing the page.
        event.preventDefault();

        // Get data from the form.
        const filters = {
            name: event.target.name.value,
            episode: event.target.episode.value,
        };

        const res = `${EPISODE}${ filter(filters) }`;
        fetchResults(res);

    };
    return(

        <>
            <form onSubmit={handleSubmit} >
                <div className={ styles_filter.filter }>
                    <h1>Episode's Filter</h1>
                    <div className={ styles_filter.filter_form}>
                        <div>
                            <div className={ styles_filter.filter_div_input}><p>Name:</p><input type="text" id="name" name="name" /></div>
                            <div className={ styles_filter.filter_div_input}><p>Episode:</p><input type="text" id="episode" name="episode" /></div>
                        </div>
                    </div>
                </div>
                <div className={styles_filter.search}>
                    <button type="submit">Search</button>
                </div>
            </form>
            
            <div className={results.noresults ? styles_card_container.main_no_results : styles_card_container.main_card}>
                <FilterResults pageProps={results} pagination={nextPrevPagination}>
                    <EpisodeCard />
                </FilterResults>
            </div>
        </>
    )

}