import Image from "next/image";
import Link from "next/link";

import styles from "@/styles/Card.module.css";

const CHARACTER = process.env.NEXT_PUBLIC_CHARACTER || "";

export function Card({ children }) {
  return (
    <div className={styles.card}>
        { children }
    </div>
  );
}

export const CharacterCard = ({ pageProps }) => {
  return (
    <Card>
        <Image src={`${CHARACTER}avatar/${pageProps.id}.jpeg`} width={140} height={140} alt={pageProps.name} priority="true"/>
        <h3>{pageProps.name}</h3>
        <Link href={`/character/${pageProps.id}`}>Detalhes</Link>
    </Card>
  );
}

export const LocationCard = function ({ pageProps }) {
  return (
      <Card>
          <h3>{pageProps.name}</h3>
          <p>Type: {pageProps.type}</p>
          <p>{pageProps.dimension}</p>
          <Link href={`/location/${pageProps.id}`}>Detalhes</Link>
      </Card>
  );
}

export const EpisodeCard = function ({ pageProps }) {
  return (
      <Card>
          <h3>{pageProps.name}</h3>
          <p>Air Date: {pageProps.air_date}</p>
          <p>Episode: {pageProps.episode}</p>
          <Link href={`/episode/${pageProps.id}`}>Detalhes</Link>
      </Card>
  );
}
