import styles from "@/styles/FilterResults.module.css"


function renderPageResults(children, pageProps, pagination){
    let page = "";
    if(pageProps?.results){
        page = (

                <>
                    <div className={styles.pagination_results}>
                        <button onClick={pageProps.info.prev ? () => pagination(pageProps.info.prev) : null}>Preview</button>
                        <button onClick={pageProps.info.next ? () => pagination(pageProps.info.next) : null}>Next</button>
                    </div>
                    <div className={styles.filter_results_card}>
                    {
                        pageProps.results.map((props) => (
                            <children.type key={props.id} pageProps={props} />
                        ))
                    }
                    </div>
                


                </>

        );
    }else if(pageProps?.noresults){
        page =  (<div> <h1>{ pageProps.noresults }</h1> </div>);
    }
    return page;
}

export default function FilterResults({ children, pageProps, pagination }){
    return (
        <>
            {
                renderPageResults(children, pageProps, pagination)
            }
        </>
    );
}