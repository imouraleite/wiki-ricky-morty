import Link from "next/link";
import Image from "next/image";

import styles from "../styles/Navbar.module.css";

export default function Navbar({data}) {
  return (
    <nav className={styles.navbar}>
      <div className={styles.logo}>
        <Image
          src="/images/logo_principal.png"
          width="70"
          height="70"
          alt="PokeNext"
        />
        { data === "Home" ? ("") : 
            (<Link href="/">
                <h1>Ricky and Morty Wiki</h1>
            </Link>)
        }
        
      </div>
      <ul className={styles.link_items}>
        <li>
          <Link href="/character">
            Characters
          </Link>
        </li>
        <li>
          <Link href="/location">
            Locations
          </Link>
        </li>
        <li>
          <Link href="/episode">
            Episodes
          </Link>
        </li>
        <li>
          <Link href="/about">
            About Me
          </Link>
        </li>
      </ul>
    </nav>
  );
}
