import styles from "../styles/Footer.module.css";

export default function Footer() {
  return (
    <footer className={styles.footer}>
      <p>
        <span>Ricky and Morty</span> &copy; 2023
      </p>
    </footer>
  );
}
