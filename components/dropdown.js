
import styles from "../styles/Dropdown.module.css"

export default function dropdown({ options, name }){
    return (
        <div className={ styles.dropdown}>
            <p>{ name }:</p>
            <select id={ name } name={ name }>
                <option value="">Choose a { name }</option>
                {
                    options.map(
                        ( option, index ) => <option key={ index } value={ option }>{ option }</option>
                    )
                }
            </select>
        </div>
    );
}

