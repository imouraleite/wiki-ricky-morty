import Navbar from "./navbar";
import Footer from "./footer";

export default function MainContainer({data, children}){
    return (
    <>
      <Navbar data={data}/>
      <div>{children}</div>
      <Footer />
    </>
  );
}
